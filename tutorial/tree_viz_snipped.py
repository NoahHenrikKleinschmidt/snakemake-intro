"""
This code snippet can visualise a newick tree in a Jupyter notebook.
"""
from ete3 import Tree

# Load a newick tree
t = Tree(<newick_tree_string>)

# Save the tree as a pdf
t.render(<output_file_name>, w=200, units="mm")

# Visualise the tree
t.show()

