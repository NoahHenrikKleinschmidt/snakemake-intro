rule my_first_rule:
    input: 
        "example_data/first.txt"
    output: 
        "first.multiline.txt"
    shell:
        """
        cat {input} | tr " " "\n" > {output}
        """

rule count_letter:
    input:
        "first.multiline.txt"
    output:
        "first.{letter}.counts"
    shell:
        """
        grep -c "{wildcards.letter}" {input} > {output}
        """

rule ruleA:
    input:
        "example_data/first.txt"
    output:
        "first.ruleA.output"
    shell:
        """
        echo "RuleA output" > {output}
        """

rule ruleB:
    input:
        rules.ruleA.output
    output:
        str(rules.ruleA.output).replace("ruleA", "ruleB")
    shell:
        """
        cat {input} > {output}
        echo "RuleB output" >> {output}
        """