# snakemake-intro

---

This repository provides a super brief introduction to `snakemake` since so many of you, my lovely co-students, keep bugging me about it...

First of all, the basic [rules documentation](https://snakemake.readthedocs.io/en/stable/snakefiles/rules.html) is actually a really good starting point for writing snakemake workflows. Then the [basics tutorial](https://snakemake.readthedocs.io/en/stable/tutorial/basics.html) is also pretty good (but I know its too much text so I didn't bother reading it either). This intro will cover some fundamentals - it turned out more text than I thought but feel free to browse through it 🌼

### Contents

1. The basic ideas behind `snakemake`
2. Writing `rules` 
3. Using `wildcards`
4. linking rules
5. extending snakemake with scripts
6. using `conda` and snakemake
7. combining `snakemake` with `SLURM`
8. Tutorial


---

## 1. The basics

A data analysis pipeline usually makes use of _many different tools_ - the outputs of which are used by again more tools. To avoid writing a single script and hard-code everything we do, we can "divide" the workflow of a pipeline into **single steps** and let a **workflow manager** (like `snakemake`) figure out *how to connect the different steps* to produce some _output_ we like. Snakemake performs it's actions through _filename_-pattern matching to decide the order of steps to execute. It essentially performs backward-regression until it reaches some existing files it can work with. 

A _snakemake pipeline_ requires a so-called `Snakefile` (needs to be named like this for automatic detection) wherein the workflow-steps are defined. The _snakefile_ is actually just an **extended `python`-file** so we can use regular python coding in there as well! 

We can _call a snakemake pipeline_ in the command line using

```
snakemake <the final output file we want to make> [--some additional options]
```

> To first do a "sanity check" on a pipeline it is good practice to do a "dry-run" by providing the `-n` option when calling the pipeline. This will simply present snakemake's "battle-plan" and exit. We can also automatically get a visual representation of the workflow using the `--dag` or `--rulegraph` options which will yield a graphviz-dot string of the entire workflow that can be rendered into an image with some appropriate tool. 

## 2. Writing `rules`

Snakemake **calls individual pipeline steps `rules`**. Each `rule` requires at least either some `intput` file(s) and/or some `output` file(s) it produces. The provided filepaths **are relative** to the snakefile! 

Each rule also performs some _action_ to produce the output using the input. Actions performed by rules can be (1) `shell`-commands just like we would write in a bash-script. (2) `python`-code just like we would write in a normal python file or jupyter notebook. (3) External `scripts` in either `python`, `R`, `bash`, or `julia`. (4) Calling actual `jupyter` notebooks. More on points 3 and 4 later.

Here is a simple example of a simple rule. It will take a (so far hardcoded) input file, perform a shell command, and thereby generate an output file. Note how all commands and stuff are inside python-strings - so we can use normal `f-string` syntax to fill in values!

```python
rule my_first_rule:
    input: 
        "example_data/first.txt"
    output: 
        "first.multiline.txt"
    shell:
        """
        cat {input} | tr " " "\n" > {output}
        """
```

> Try making a `Snakefile` in this directory with this rule and call the pipeline.
>

If we would rather let a rule do some basic python stuff we can either make a real script for it and refer the rule to that or use the `run` directive and write the code directly there:

```py
rule runs_some_python:
    input:
        "first.multiline.txt"
    output:
        "second.multiline.txt"
    run:
        with open(input) as f:
            content = f.read()
        do more stuff here...
```
> Note how this is **not** inside a string!

#### Additional attributes

Apart from `input` and `output` rules also support other attributes such as generic `params` (parameters), `resources` (like parameters but specified to computational resources like memory), `threads` (number of maximum threads), and `log` (for outputting logs not to `stdout` but somewhere else). 

We can make a second rule to count the number of 'i' in the file `first.multiline.txt` (because why not and I don't want to think of a more useful example right now)... This rule also specifies the letter we want to count as a parameter (this makes it easier to edit).

```py
rule count_i:
    input: 
        "first.multiline.txt"
    output: 
        "first.counts"
    params:
        letter = "i"
    shell:     # we can use params.<whatever> to access the parameters in our shell command
        """    #   /
        grep -c "{params.letter}" {input} > {output}
        """
```

> You are getting the hang of this by now...
> By the way, what are `params` useful for? We can load default settings from a config file (maybe load the config into a dictionary) and the use normal python syntax to assign 
> ```
> params:
>   letter = config["letter"]
> ```
> or something like this to make our pipeline more versatile...

#### Multiple input/output files 

We are of course not restricted to a single input and output but can have any number of them. The only thing we need to keep in mind is that snakemake **really wants trailing commas!!!**:

```py
rule many_inputs_with_names:
    input:
        fasta = "something.fasta", # <-- notice the TRAILING COMMAS - SUPER IMPORTANT !!!
        gff = "something.gff",
    output:
        "superfile.out"
    shell:
        """
        command -fi {input.fasta} --gff {input.gff} -o {output}
        """

rule many_inputs_without_names:
    input:
        "something.fasta", # <-- notice the TRAILING COMMAS !!!
        "something.gff",
    output:
        "superfile.out"
    shell:
        """
        command -fi {input[0]} --gff {input[1]} -o {output}
        """
```
> The same logic applies to `output` as well... 
> 
 
#### Output directories 
By the way, if we want a directory as output instead of a file we have to specify:

```
output:
  directory("my_outdir")
```
otherwise we get an error...

#### Intermediary outputs

If we know that some file is only going to be needed for a short time we can specify it as a temporary file using 

```
output:
  temp("intermediary_file")
```

this will cause snakemake to delete it once the file is no longer needed...
## 3. Using `wildcards`

This is really at the *heart* of how snakemake operates. We can specify some wildcards in our input and output filenames and snakemake will then figure these out from your actual pipeline call. 

Imagine we are trying to count not only 'i' but any kind of letter. We can specify a rule that looks like this:

```py
rule count_letter:
    input:
        "first.multiline.txt"
    output:
        "first.{letter}.counts"
    shell:
        """
        grep -c "{wildcards.letter}" {input} > {output}
        """
```

now we can use our pipeline to produce for instance

```
snakemake first.h.counts -j 1 # will count the letter "h"
or
snakemake first.e.counts -j 1 # will count the letter "e"
```

Wildcards are available directly in the input and output filenames and via `wildcards.<whatever>` in the `shell` or `run` directives. 

## Expanding wildcards

Assume we know that we are trying to fill-in some wildcards with values while keeping others as wildcards. We can use default python string formatting to do that, together with the snakemake function `expand`. Assume we have two fastq files in forward and reverse direction. 

```py
DIRECTIONS = ("R1", "R2") # normal python tuple

rule something_with_fastq:
    input:
        expand("raw_files/{{dataset}}.{direction}.fastq", direction = DIRECTIONS) 
        # notice the double-{{brackets}} for anything that should NOT be filled in...
    ...
```
> This will turn the `input` into a list of `["raw_files/{dataset}.R1.fastq", "raw_files/{dataset}.R2.fastq"]`


## 4. Linking Rules

You have probably already grasped the concept of "output from ruleA is input for ruleB" - that's what pipelines do... To avoid having to copy/paste the actual strings all the time, we can access the `rules` and their attributes in a snakefile directly. To do so simply do `rules.<rule name>.<whatever attribute>[.<maybe subattribute>]`. 

For instance check this out:

```py
rule ruleA:
    input:
        "example_data/first.txt"
    output:
        "first.ruleA"
    shell:
        """
        echo "RuleA output" > {output}
        """

rule ruleB:
    input:
        rules.ruleA.output
    output:
        str(rules.ruleA.output).replace("ruleA", "ruleB")
    shell:
        """
        cat {input} > {output}
        echo "RuleB output" >> {output}
        """
```
> Here we are connecting the output from ruleA as the input to ruleB. Then we are using some normal python code to adjust the output filename for ruleB. The same thing also works for any other attribute like `params` etc. By the way, since the attributes of rules are some snakemake-specific datatypes we usually need to convert them using like in this case with `str()`...
>

### Multiple Snakefiles

While we are on the subject, we can write multiple snakefiles just like python modules. It is customary to call these `some_snakefile.smk`. These can be "imported" into the main `Snakefile` using 

```py
# at the top of the file (remember to use relative paths!)
include: "my_snakefiles/fileA.smk"
include: "my_snakefiles/fileB.smk"
```

## 5. Extending snakemake with scripts

Sometimes it is inconvenient to use `run` directly in the snakefile or we want to use `R` for our analysis. In this case we can use the `script` directive to tell a snakemake rule to call a specific script. The snakemake docs are really nice on this bit so [check them out here](https://snakemake.readthedocs.io/en/stable/snakefiles/rules.html#external-scripts). 

To make use of for example a python script we first write the script. While doing so we can use the "magic" `snakemake` object which will be available when the script is called by snakemake later on - however, it will spring a ton of "undefined variable" warnings when writing...


```py
# the script (let's call it data_repeater.py)

# magic use of the snakemake object which holds all
# attributes we need (even though we never import it or anything...)

infile = snakemake.input
outfile = snakemake.output
repeats = int(snakemake.wildcards.repeats)

import pandas as pd

df = pd.read_csv(infile, header = None, sep = " ")

df = pd.concat(repeats*[df])

df.to_csv(outfile, sep = " ", index = False)
```

```py
# the snakemake rule
rule call_my_script:
    input:
        "example_data/first.txt"
    output:
        "first.{repeats}_repeated.txt"
    script:
        "my_scripts/data_repeater.py"
```
> **Note** because snakemake inserts the magic `snakemake` object into the scripts, the scripts do **not** use command-line arguments!!! 


#### Jupyter notebooks

The same magic `snakemake` object is also passed to jupyter notebooks, so we can also prepare a notebook (maybe for making a plot that we later want to adjust a bit). To actually also save a notebook snakemake requires a little workaround (no idea why):

```py
rule use_jupyter_notebook:
    input:
        "<some input>"
    output:"
        "<some output>" # NOT the jupyter notebook but some other file !!!
    log:
        notebook = "<the output jupyter notebook>", # the notebook containing the final code from the rule 
    notebook:
        "my_notebooks/<the notebook you want to call>.py.ipynb" # the template notebook you prepared before
```
> Looks a bit ugly but there there you go... By the way because jupyter supports both python and R, snakemake asks you to use `.py.ipynb` or `.r.ipynb` as suffixes for the prepared notebooks.
>

# 6. `conda` and snakemake

We often want to use different tools that are incompatible with each other (or distributed in some shitty container that nobody knows how to work with because there is no documentation for it). Luckily, most bioinformatic software tools are actually available via `bioconda` and can be installed into separate `conda enironments`. Snakemake rules support a `conda` directive which links to an `environment.yaml` file that describes the envirnoment to use. Check out the [conda documentation](https://conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html#create-env-file-manually) to learn more about the yaml file stuff, they essentially look like this:

```yaml
name: blast_env
channels:
    - bioconda
    - conda-forge
dependencies:
    - blast
```
> to make an environment that has BLAST installed...

```py
rule do_blast:
    input:
        "my_cool_input.file"
    output:
        "my_cooler_output.file"
    conda:
        "envs/my_cool_env.yaml"
    shell:
        """
        produce cool {output}
        """
```
> This will create the respective environment (if not already created). By the way, `conda` is only available for rules having the `shell` or `script`/`notebook` directive, but not for rules using `run`!

To then also tell snakemake to use dedicated conda environments we need to pass the option `--use-conda` when calling the pipeline. 

```
snakemake my_cooler_output.file -j 1 --use-conda
```

## 7. Using `SLURM`

We can tell snakemake to _submit sbatch jobs_ using the `--cluster` option when calling the pipeline. Like so:

```
snakemake <big output stuff> --cluster="sbatch <options here>" --jobs 10
```

This even supports wildcards in the `--cluster` option string that are specific to rules:

```py
rule do_something:
    input:
        "<some input>"
    output:
        "<some output>"
    threads:
        20
    resources:
        mem_mb="5G",
    shell:
        """
        do stuff to make {output}
        """
```
        
so when calling the pipeline we can use

```
snakemake <big output stuff> --cluster="sbatch --job-name='{name}' --cpus-per-task={threads} --mem={resources.mem_mb}" --jobs 10
```

to fill in the blanks according to whatever is specified by the respective rule. Like this we can dynamically allocate job resources to what we actually need.

### Exluding rules from SLURM

Some rules are computationally easy so a job-submission would be unnecessary and just clog up the queue. For this case snakemake allows us to specify `localrules` which shall not be submitted to the cluster:

```py
localrules:
    ruleA
    ruleB
    ...

rule ruleA:
    input:
        ...
    
rule ruleB:
    ...
```


---

Alrighty, that's it for this brief overview. I suggest you just think of some simple testing workflow to play around with so you get the hang of it... Good luck ☀️

---

### 8. Your first pipeline 

If you want to test out making a workflow, there is a file `uniprot_upf1.fasta`. We will now make a pipeline that

1. Extracts all sequences that come from _Human_ proteins:

2. Performs multiple-sequence alignment using `clustalw`

3. Generates a phylogenetic tree using `fasttree` 

4. Generates a _jupyter notebook_ that loads the output _newick_ tree from `fasttree` (ideally you install `ete3` and use the provided code in the `tutorial` directory).

> By the way, either you install everything locally or you work on the IBU Cluster...
>

